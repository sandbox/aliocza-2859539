<?php

namespace Drupal\ckeditor_extends\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginContextualInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the "skins" plugin.
 *
 * @CKEditorPlugin(
 *   id = "skins",
 *   label = @Translation("Skins")
 * )
 */
class Skins extends CKEditorPluginBase implements ContainerFactoryPluginInterface, CKEditorPluginContextualInterface, CKEditorPluginConfigurableInterface {

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    // This plugin is already part of Drupal core's CKEditor build.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {
    // This plugin represents the core CKEditor plugins. They're always enabled:
    // its configuration is always necessary.
    return TRUE;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
	    $config = [];
		$settings = $editor->getSettings();
		//$config['skin'] = 'moonocolor,/'.drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/skins/moonocolor/';

        $skin = $settings['plugins']['skins']['skins'];
        if ($skin) {
            $config['skin'] = $skin.','.$GLOBALS['base_path'].drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/skins/'.$skin.'/';
        }

        return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
    $settings = $editor->getSettings();


    $form['skins'] = array(
      '#title' => t('Skin'),
      '#title_display' => 'visible',
      '#type' => 'select',
      '#options' => array(
						'atlas' => 'Atlas',
						'bootstrapck' => 'Bootstrapck',
						'flat' => 'Flat',
						'icy_orange' => 'Icy orange',
						'kama' => 'Kama',
						'minimalist' => 'Minimalist',
						'moono' => 'Moono',
						'moono_blue' => 'Moono blue',
						'moonocolor' => 'Moono color',
						'moono-dark' => 'Moono dark',
						'moono-lisa' => 'Moono lisa',
						'office2013' => 'Office 2013',
					),
       '#default_value' => !empty($settings['plugins']['skins']['skins']) ? $settings['plugins']['skins']['skins'] : 'Moono',
       '#description' => t('Select your skin.'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
        return [];
  }

    /**
     * Ensure values entered is boolean
     * @param $element
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     */
    public function validateInput(array $element, FormStateInterface $form_state) {

    }







}
