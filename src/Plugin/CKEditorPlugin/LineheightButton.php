<?php

namespace Drupal\ckeditor_extends\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "lineheight" plugin.
 *
 * @CKEditorPlugin(
 *   id = "lineheight",
 *   label = @Translation("Line height Button")
 * )
 */
class LineheightButton extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

    /**
     * {@inheritdoc}
     */
    public function getDependencies(Editor $editor) {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getFile() {
        return drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/plugins/lineheight/plugin.js';
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(Editor $editor) {
  
        $settings = $editor->getSettings();
		

		$config = [
		  //'line_height' => "1em;1.1em;1.2em;1.3em;1.4em;1.5em",
		];
        
        return $config;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
        $settings = $editor->getSettings();

            return array();
    }

    /**
     * {@inheritdoc}
     */
    public function getButtons() {
        return [
            'lineheight' => array(
                'label' => t('Line height'),
                'image_alternative' => [
                  '#type' => 'inline_template',
                  '#template' => '<a href="#" role="button" aria-label="{{ styles_text }}"><span class="ckeditor-button-dropdown">{{ styles_text }}<span class="ckeditor-button-arrow"></span></span></a>',
                  '#context' => [
                    'styles_text' => t('Line height'),
                  ],
                ],
            ),
        ];
    }

}
