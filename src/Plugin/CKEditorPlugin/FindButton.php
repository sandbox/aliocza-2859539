<?php

namespace Drupal\ckeditor_extends\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "find" plugin.
 *
 * @CKEditorPlugin(
 *   id = "find",
 *   label = @Translation("Find Button")
 * )
 */
class FindButton extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/plugins/find/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = [];


    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'Find' => array(
        'label' => $this->t('Find'),
        'image' => drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/plugins/find/icons/find.png',
      ),
      'Replace' => array(
        'label' => $this->t('Replace'),
        'image' => drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/plugins/find/icons/replace.png',
      ),
    ];
  }


  
}