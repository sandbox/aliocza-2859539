<?php

namespace Drupal\ckeditor_extends\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "selectall" plugin.
 *
 * @CKEditorPlugin(
 *   id = "selectall",
 *   label = @Translation("Select all Button")
 * )
 */
class SelectallButton extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/plugins/selectall/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = [];


    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'SelectAll' => array(
        'label' => $this->t('Select all'),
        'image' => drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/plugins/selectall/icons/selectall.png',
      ),
    ];
  }


  
}