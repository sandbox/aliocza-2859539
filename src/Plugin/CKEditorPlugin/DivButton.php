<?php

namespace Drupal\ckeditor_extends\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "div" plugin.
 *
 * @CKEditorPlugin(
 *   id = "div",
 *   label = @Translation("Div Button")
 * )
 */
class DivButton extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

    /**
     * {@inheritdoc}
     */
    public function getDependencies(Editor $editor) {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getFile() {
        return drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/plugins/div/plugin.js';
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(Editor $editor) {
        $settings = $editor->getSettings();

        if ($settings['plugins']['div']['div_wrapTable'] === 1) {
            $config = [
                'div_wrapTable' => true,
            ];
        } else {
            $config = [
                'div_wrapTable' => false,
            ];
        }

        return $config;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
        $settings = $editor->getSettings();

        $form['div_wrapTable'] = array(
            '#type' => 'checkbox',
            '#title' => $this->t('div_wrapTable'),
            '#description' => $this->t('Whether to wrap the entire table instead of individual cells when creating a &lt;div&gt; in a table cell.'),
            '#default_value' => !empty($settings['plugins']['div']['div_wrapTable']) ? $settings['plugins']['div']['div_wrapTable'] : 0,
        );

        $form['div']['#element_validate'][] = array($this, 'validateInput');

        return $form;
    }

    /**
     * Ensure values entered is boolean
     * @param $element
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     */
    public function validateInput(array $element, FormStateInterface $form_state) {
        $input = $form_state->getValue(['editor', 'settings', 'plugins', 'div', 'div_wrapTable']);

        if (!preg_match('/([0-1]{1})/i', $input)) {
            $form_state->setError($element, 'Only valid boolean values are allowed (0-1). Please check your settings for div and try again.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getButtons() {
        return [
            'CreateDiv' => array(
                'label' => $this->t('Div container'),
                'image' => drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/plugins/div/icons/creatediv.png',
            ),
        ];
    }

}
