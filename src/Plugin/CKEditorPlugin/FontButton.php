<?php

namespace Drupal\ckeditor_extends\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "font" plugin.
 *
 * @CKEditorPlugin(
 *   id = "font",
 *   label = @Translation("Font Button")
 * )
 */
class FontButton extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {

    /**
     * {@inheritdoc}
     */
    public function getDependencies(Editor $editor) {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getFile() {
        return drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/plugins/font/plugin.js';
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(Editor $editor) {
        global $base_url;
        $settings = $editor->getSettings();

        
        return array();
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {
        $settings = $editor->getSettings();

            return array();
    }

    /**
     * {@inheritdoc}
     */
    public function getButtons() {
        return [
            'Font' => array(
                'label' => t('Font'),
                'image_alternative' => [
                  '#type' => 'inline_template',
                  '#template' => '<a href="#" role="button" aria-label="{{ styles_text }}"><span class="ckeditor-button-dropdown">{{ styles_text }}<span class="ckeditor-button-arrow"></span></span></a>',
                  '#context' => [
                    'styles_text' => t('Font'),
                  ],
                ],
            ),
            'FontSize' => array(
                'label' => t('Font Size'),
                'image_alternative' => [
                  '#type' => 'inline_template',
                  '#template' => '<a href="#" role="button" aria-label="{{ styles_text }}"><span class="ckeditor-button-dropdown">{{ styles_text }}<span class="ckeditor-button-arrow"></span></span></a>',
                  '#context' => [
                    'styles_text' => t('Font Size'),
                  ],
                ],
            ),
        ];
    }

}
