<?php

namespace Drupal\ckeditor_extends\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;
use Drupal\Component\Utility\UrlHelper;

/**
 * Defines the "templates" plugin.
 *
 * @CKEditorPlugin(
 *   id = "templates",
 *   label = @Translation("Templates Button")
 * )
 */
class TemplatesButton extends CKEditorPluginBase implements CKEditorPluginConfigurableInterface {
	private $pluginPath;

	public function __construct(){

		if(file_exists(DRUPAL_ROOT.'/libraries/ckeditor/plugins/templates/plugin.js')){
			$this->pluginPath = 'libraries/ckeditor/plugins/templates';
		}else{
			$this->pluginPath = drupal_get_path('module', 'ckeditor_extends').'/vendor/ckeditor/plugins/templates';
		}
	}

    /**
     * {@inheritdoc}
     */
    public function getDependencies(Editor $editor) {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getFile() {

        return $this->pluginPath.'/plugin.js';
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig(Editor $editor) {
        $settings = $editor->getSettings();
        $config = array();

        if ($settings['plugins']['templates']['templates_replaceContent'] === 1) {
            $config['templates_replaceContent'] = true;
        } else {
            $config['templates_replaceContent'] = false;
        }
				if($settings['plugins']['templates']['templates_files']){
					$config['templates_files'] = [$settings['plugins']['templates']['templates_files']];
				}else{
    	  	$config['templates_files'] = [$GLOBALS['base_path'].$this->pluginPath.'/templates/default.js'];
			  }

        return $config;
    }

    /**
     * {@inheritdoc}
     */
    public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {

        $settings = $editor->getSettings();

        $templates_replaceContent =  $settings['plugins']['templates']['templates_replaceContent'] === 1 ? 1 : 0;
        $form['templates_replaceContent'] = array(
            '#type' => 'checkbox',
            '#title' => $this->t('templates_replaceContent'),
            '#description' => $this->t('Whether the "Replace actual contents" checkbox is checked by default in the Templates dialog.'),
            '#default_value' => $templates_replaceContent,
						'#element_validate' =>[ array($this, 'validateReplaceContent')],
        );
		  	$templates_files =  $settings['plugins']['templates']['templates_files'] ? : '';
				$form['templates_files'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('templates_files'),
            '#description' => $this->t('The list of templates definition files to load.'),
            '#default_value' => $templates_files,
						'#element_validate' => [array($this, 'validateFiles')],
        );

        return $form;
    }

    /**
     * Ensure values entered is boolean
     * @param $element
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     */
    public function validateReplaceContent(array $element, FormStateInterface $form_state) {
        $templates_replaceContent = $form_state->getValue(['editor', 'settings', 'plugins', 'templates', 'templates_replaceContent']);
        if (!preg_match('/([0-1]{1})/i', $templates_replaceContent)) {
              $form_state->setError($element, 'Only valid boolean values are allowed (0-1). Please check your settings for templates and try again.');
        }
    }

		/**
     * Ensure values entered is valide url or file exist
     * @param $element
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     */
    public function validateFiles(array $element, FormStateInterface $form_state) {
				$templates_files = $form_state->getValue(['editor', 'settings', 'plugins', 'templates', 'templates_files']);

				$templates_files_valide = UrlHelper::isValid($templates_files, $absolute = TRUE);
				if(!$templates_files_valide and !file_exists(DRUPAL_ROOT.$templates_files)){
					 $form_state->setError($element, 'Invalide url or file not found. Pleack check your settings for templates file and try again .');
				}
    }


    /**
     * {@inheritdoc}
     */
    public function getButtons() {
        return [
            'Templates' => array(
                'label' => $this->t('Templates'),
                'image' => $this->pluginPath.'/icons/templates.png',
            ),
        ];
    }

}
